# TicTacToe-Qt

Réalisation d'un TicTactToe en Qt :

Concept manipulés : 
- Les signaux et les slots
- QGridLayout
- QpushButton
- QMainWindow -  Qui est une classe généré par Qt Creator lorsque l'on souhaite créer un projet de type widget
- Initiation au QtDesigner qui permet via un Wyziwig :
    - De monter des interfaces
    - De faire code managé comme avec les MFC , 
    - Le QtDesigner va créer les slots dans le header et l'implémentation dans le CPP directement en connectant les slots.
    - Si on souhaite avoir la main sur les slots ont va créer les éléments graphiques directement dans les classes (en dehor du constructeur de MainWindow ^^ )
- Nous avons fait connaissance avec QNetWorkAccessManager , QWebengine
- Découverte des différentes dépendances de Qt 
- Découverte des ./configure, MakeFile, make CF : Doc Client lourd RNCP pour CDA
    
TODO : 
- Compiler avec ou sans les autotools, les différentes librairies : 
    - QSqlDatabase
    - webengine
    - testlib
    - NetworkAccessManager
    - QML Freebox compiler son environnment de DEV pour Freebox : 
    - https://dev.freebox.fr/sdk/player.html
    


