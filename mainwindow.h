#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void SetBoardDepth(int);

public slots:
    void changeTicTacToeDisplay();

private:
    Ui::MainWindow *ui;
    bool bTurn;
    void CheckTicTacToeBoardStates();
    void PersistTicTacToeData();
    void ReinitializeTicTacToe();
    QList <QPushButton*> GetCentralWidget();
    void GetTicTacToeState();
    void InitTicTacToeBoard();
    int BoardDepth;
    QVector <QString > TicTacToeState;
    QVector< QVector <QVector <QString > > > StateVector;
};
#endif // MAINWINDOW_H
