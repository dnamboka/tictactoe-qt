#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "tictactoeengine.h"
#include <QGridLayout>
#include <QPushButton>
#include <QMessageBox>
#include <QDebug>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QStateMachine>
#include <QState>
#include <QSqlError>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui -> setupUi(this);
    this -> InitTicTacToeBoard();
}

void MainWindow::InitTicTacToeBoard(){
    this -> bTurn = false;
    QWidget* widget = new QWidget(this);
    QGridLayout * TicTacToeGrid =  new QGridLayout( widget );
    const QSize BUTTON_SIZE = QSize(210, 210);
    int iterator = 0;
    this -> SetBoardDepth( 3 );

    for( int j = 0; j < this -> BoardDepth; j++ )
    {
        while( iterator < this -> BoardDepth )
        {
            QPushButton * pushbutton = new QPushButton();
            pushbutton -> setMinimumSize( BUTTON_SIZE );
            pushbutton -> setStyleSheet( "color: white;" "font-size:100px;" "font-family:Arial" );
            TicTacToeGrid -> addWidget( pushbutton, j , iterator);
            connect( pushbutton, &QPushButton::clicked,this , &MainWindow::changeTicTacToeDisplay);
            iterator++;
        }
        iterator = 0;
    }

    widget->setLayout( TicTacToeGrid );
    this -> setCentralWidget( widget );
}

void MainWindow::PersistTicTacToeData(){
    QSqlDatabase db;
    QSqlQuery query;

    db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName( "127.0.0.1" );
    db.setPort(8889);
    db.setDatabaseName("moodle");
    db.setUserName( "root" );
    db.setPassword( "root" );

    if (!db.open())
    {
       qDebug() << db.lastError();
    }
    else
    {
       qDebug() << "DB Open !";

       //QSqlQuery query("INSERT INTO mdl_analytics_models VALUES (NULL , 0, 0, NULL, '\core_course\analytics\target\no_recent_accesses', '[\"\\core\\analytics\\indicator\\any_course_access\"]', '\core\analytics\time_splitting\past_month', NULL, 1609883207, NULL, 1609883207, 1609883207, 0);");
       QSqlQuery query( "SELECT * FROM mdl_analytics_models" );
       while (query.next()) {
           QString target = query.value( "target" ).toString();
           QString indicators = query.value( "indicators" ).toString();
           QString timecreated = query.value( "timecreated" ).toString();
           qDebug() << target <<" "<< indicators << " " << timecreated;
       }
    }
}

void MainWindow::CheckTicTacToeBoardStates(){

    // Get the Winner
    QMessageBox msgBox;
    QString WinMessage("Tic Tac Toe Winner ! ");
    if( this -> bTurn )
        WinMessage.append( "X" );
    else
        WinMessage.append( "0" );
    msgBox.setText( WinMessage );

    this -> GetTicTacToeState();

    if ( this -> TicTacToeState[0] !="" && this->TicTacToeState[0] == this->TicTacToeState[1] && this->TicTacToeState[1] == this->TicTacToeState[2]){
        this->PersistTicTacToeData();
        msgBox.exec();
        this -> ReinitializeTicTacToe();
    }
    else if ( this -> TicTacToeState[ 3 ] != "" && this -> TicTacToeState[3] == this -> TicTacToeState[4] && this -> TicTacToeState[4] == this -> TicTacToeState[5]) {
        this->PersistTicTacToeData();
        msgBox.exec();
        this -> ReinitializeTicTacToe();
    }

    else if ( this -> TicTacToeState[6] !="" && this -> TicTacToeState[6] == this -> TicTacToeState[7] && this -> TicTacToeState[7] == this -> TicTacToeState[8] ){
        this->PersistTicTacToeData();
        msgBox.exec();
        this -> ReinitializeTicTacToe();
    }

    else if ( this -> TicTacToeState[0] !="" && this -> TicTacToeState[0] == this -> TicTacToeState[3] && this -> TicTacToeState[3] == this -> TicTacToeState[6] ){
        this->PersistTicTacToeData();
        msgBox.exec();
        this -> ReinitializeTicTacToe();
    }

    else if ( this -> TicTacToeState[1] !="" && this -> TicTacToeState[1] == this -> TicTacToeState[4] && this -> TicTacToeState[4] == this -> TicTacToeState[7] ){
        this->PersistTicTacToeData();
        msgBox.exec();
        this -> ReinitializeTicTacToe();
    }

    else if ( this -> TicTacToeState[2] !="" && this -> TicTacToeState[2] == this -> TicTacToeState[5] && this -> TicTacToeState[5] == this -> TicTacToeState[8] ){
        this->PersistTicTacToeData();
        msgBox.exec();
        this -> ReinitializeTicTacToe();
    }

    else if ( this -> TicTacToeState[0] !="" && this -> TicTacToeState[0] == this -> TicTacToeState[4] && this -> TicTacToeState[4] == this -> TicTacToeState[8] ){
        this->PersistTicTacToeData();
        msgBox.exec();
        this -> ReinitializeTicTacToe();
    }

    else if ( this -> TicTacToeState[2] !="" && this -> TicTacToeState[2] == this -> TicTacToeState[4] && this -> TicTacToeState[4] == this -> TicTacToeState[6] ){
        this->PersistTicTacToeData();
        msgBox.exec();
        this -> ReinitializeTicTacToe();
    }

    TicTacToeState.clear();
}

QList<QPushButton*> MainWindow::GetCentralWidget(){
    QList< QPushButton* > pushbuttons = this -> centralWidget() -> findChildren < QPushButton * >();
    return pushbuttons;
}

void MainWindow::ReinitializeTicTacToe(){
    foreach ( QPushButton * pbutton, this -> GetCentralWidget() ){
       pbutton -> setText("");
       pbutton -> setDisabled(false);
    }
}

void MainWindow::GetTicTacToeState(){
    foreach ( QPushButton * pbutton, this -> GetCentralWidget() ){
       this -> TicTacToeState.append( pbutton -> text() );
    }
}
void MainWindow::SetBoardDepth( int iDepth ){
    this -> BoardDepth = iDepth;
}

void MainWindow::changeTicTacToeDisplay()
{
    QPushButton* buttonSender = qobject_cast<QPushButton* >(sender()); // retrieve the button you have clicked
    if (buttonSender -> isEnabled())
    {
        if ( this->bTurn ){
            buttonSender->setText( "X" );
            this->bTurn = false;
        }
        else{
            buttonSender->setText( "0" );
            this->bTurn = true;
        }
    }
    buttonSender->setDisabled(true);
    this -> CheckTicTacToeBoardStates();
}

MainWindow::~MainWindow()
{
    delete ui;
}

